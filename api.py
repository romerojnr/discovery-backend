#!/usr/bin/env python
from flask import Flask
from flask import request
from flask import jsonify
from analyzer import Device, Network

from multiprocessing.pool import ThreadPool

app = Flask(__name__)

@app.route("/discovery/network/<network>/<bitmask>", methods=['GET'])
def discovery_network(network, bitmask):
    network = Network(ip=network, mask=bitmask)
    network.scan_hosts()
    network.append_attr('append_fqdn')
    network.append_attr('append_neighbors')
    return jsonify(network.json())

@app.route("/discovery/network_v2/<network>/<bitmask>", methods=['GET'])
def discovery_network_v2(network, bitmask):
    network = Network(ip=network, mask=bitmask)
    network.scan_hosts()
    network.append_attr('append_fqdn')
    network.append_attr('append_neighbors')
    return jsonify(network.json())

@app.route("/discovery/device/<addr>", methods=['GET'])
def discovery_device(addr):
    device = Device(ip=addr, community="Ek5dof1eDd0A")
    device.append_fqdn()
    device.append_neighbors()
    return jsonify(device.attr)

@app.route("/discovery/device/<addr>/portmap", methods=['GET'])
def portmap(addr):
    device = Device(ip=addr, community="Ek5dof1eDd0A")
    device.map_ports()
    return jsonify(device.portmap)

@app.route("/discovery/device/<addr>/hostname", methods=['GET'])
def hostname(addr):
    device = Device(ip=addr, community="Ek5dof1eDd0A")
    device.find_hostname()
    return jsonify(device.hostname)

@app.route("/discovery/device/<addr>/traffic/neighbors", methods=['GET'])
def traffic_neighbors(addr):
    device = Device(ip=addr, community="Ek5dof1eDd0A")
    device.map_ports()
    print "finding neighbors..."
    device.find_neighbors()
    print "found neighbors!"
    neighbor_traffic = list()
    pool = ThreadPool(processes=30)
    async_results = list()
    for neighbor in device.neighbors:
        print 'checking: %s' % neighbor
        async_results.append(pool.apply_async(device.traffic_load, (neighbor['port_index'],)))
    for i in async_results:
        for neighbor in device.neighbors:
            if neighbor['port_index'] == i.get()['port_index']:
                neighbor['traffic'] = i.get()
                neighbor_traffic.append(neighbor)
    return jsonify(neighbor_traffic)

@app.route("/discovery/device/<addr>/traffic/<port>", methods=['GET'])
def traffic_port(addr, port):
    device = Device(ip=addr, community="Ek5dof1eDd0A")
    return jsonify(device.traffic_load(port))

@app.route("/discovery/device/<addr>/neighbors", methods=['GET'])
def neighbors(addr):
    device = Device(ip=addr, community="Ek5dof1eDd0A")
    device.map_ports()
    device.find_neighbors()
    return jsonify(device.neighbors) 

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="8080")
