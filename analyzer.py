#!/usr/bin/env python
import time
import nmap
import json
import sys
import os

from threading import Thread
from Queue import Queue

from pysnmp.hlapi import *

class Device(object):
    def __init__(self, **kwargs):
        self.ip = kwargs.get('ip')
        self.community = kwargs.get('community')
        self.portmap = kwargs.get('portmap')
        self.neighbors = kwargs.get('neighbors')
        self.hostname = kwargs.get('hostname')
        self.attr = dict()
        
    def snmp_query(self, mib, target, index=False, debug=False, **kwargs):
        """
        Takes a mib and target, and returns a generator that can be 
        iterated containing the snmpwalk-like output.
        """
        if index:
            Cmd = getCmd(SnmpEngine(),
                         CommunityData(self.community),
                         UdpTransportTarget((self.ip, 161), timeout=2, retries=0),
                         ContextData(),
                         ObjectType(ObjectIdentity(mib, target, index)),
                         lexicographicMode=False,
                         lookupMib=False)
        else:
            Cmd = nextCmd(SnmpEngine(),
                         CommunityData(self.community),
                         UdpTransportTarget((self.ip, 161), timeout=2, retries=0),
                         ContextData(),
                         ObjectType(ObjectIdentity(mib, target)),
                         lexicographicMode=False,
                         lookupMib=False) 
         
        for (errorIndication, errorStatus, errorIndex, varBinds) in Cmd:
            if errorIndication:
                if debug:
                    print(errorIndication)
                break
            elif errorStatus:
                if debug:
                    print('%s at %s' % (errorStatus.prettyPrint(),
                                        errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
                break
            else:
                 for varBind in varBinds:
                     if kwargs.get('return_all'):
                        # if return_all is signaled return tuple (ObjectName, OctetString)
                        yield varBind
                     else:
                        yield varBind[1].prettyPrint()

    
    def append_attr(self,mib,target):
        """
        Parses the output from snmp_query and appends to the object
        attribute (attr) list.
        """
        self.attr[target] = [ item for item in self.snmp_query(mib, target) ]
        pass
    
    def append_neighbors(self):
        self.append_attr('LLDP-MIB', 'lldpRemSysName')
        pass
    
    def append_fqdn(self):
        self.append_attr('SNMPv2-MIB', 'sysName')
        pass
    
    def map_ports(self):
        portmap = dict()
        # for each port for the self instance
        for port in self.snmp_query("IF-MIB", "ifDescr", return_all=True):
            # add to the dict description -> index
            portmap[port[1].prettyPrinter()] = port[0].prettyPrinter().split('.')[-1] 
        # update the attribute port_map with the new values
        self.portmap = portmap
        pass

    def interface_speed(self, port):
        # gets the interface max speed (ex: 1g, 10g or 40g)
        return int(self.snmp_query('IF-MIB', 'ifHighSpeed', port).next())
     
    def traffic_load(self, port, interval=5):
        # query current values to use as refence (64-bit counters)
        in_octets_ref = int(self.snmp_query('IF-MIB', 'ifHCInOctets', port).next())
        out_octets_ref = int(self.snmp_query('IF-MIB', 'ifHCOutOctets', port).next())
        
        # slowly waits for data collection
        time.sleep(interval)
        
        # query current values for later delta calculation
        in_octets_last = int(self.snmp_query('IF-MIB', 'ifHCInOctets', port).next())
        out_octets_last = int(self.snmp_query('IF-MIB', 'ifHCOutOctets', port).next())
        
        # returns serialized json with speed in mbps
        return { 'input_rate_mbps': int(((in_octets_last - in_octets_ref)*0.000008)/interval), 
                 'output_rate_mbps': int(((out_octets_last - out_octets_ref)*0.000008)/interval),
                 'speed_mbps': self.interface_speed(port),
                 'port_index': port }
    
    def find_neighbors(self):
        neighbors = list()
        if self.portmap:
            for item in self.snmp_query("LLDP-MIB", "lldpRemSysName", return_all=True):
                # get the index used exclusively by lldp
                lldp_index = item[0].prettyPrinter().split('.')[-2]
                # with the index, get the port description, such as Ethernet1 or ge-0/0/1.0
                port_desc = self.snmp_query("LLDP-MIB", "lldpLocPortId", lldp_index).next()
                if any(c.isalpha() for c in port_desc):
                    neighbors.append({ 'port_index': self.portmap[port_desc],
                                       'hostname': item[1].prettyPrinter(),
                                       'port_desc': port_desc })
                else:
                    neighbors.append({ 'port_index': lldp_index, 
                                       'hostname': item[1].prettyPrinter(),
                                       'port_desc': port_desc })
        self.neighbors = neighbors
        pass
    
    def find_hostname(self):
        self.hostname = self.snmp_query("SNMPv2-MIB", "sysName").next()
        pass

class Network(object):
    def __init__(self, **kwargs):
        self.ip = kwargs.get('ip')
        self.mask = kwargs.get('mask')
        self.devices_up = list()
    
    def json(self):
        return [ { 'id' : idx, 'ip' : item.ip, 'attr' : item.attr } for idx, item in enumerate(self.devices_up) ]
    
    def scan_hosts(self):
        """
        Appends to the devices_up attribute all active (pingable) hosts within
        the network range.
        """
        nm = nmap.PortScanner()
        nm.scan(hosts=self.ip + '/' + self.mask, arguments="-sP")
        self.devices_up = [ Device(ip=addr, community="Ek5dof1eDd0A") for addr in nm.all_hosts() ]
        pass
    
    def append_attr(self, method):
       """
       Execute methods from Device class in bulk (threads).
       I certainly need to improve this...
       """
       threads = [ Thread(target=getattr(device, method)) for device in self.devices_up ]
       [ item.start() for item in threads ]
       [ item.join() for item in threads ]
       pass
    
if __name__ == "__main__":
    switch = Device(ip="10.13.13.79", community="Ek5dof1eDd0A")
    switch.find_hostname()
    print switch.hostname
    sys.exit() 
